﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Usuario
{
    class Usuarios
    {
        private int id;
        private string nombre;
        private string contra;
        private string conf_contra;
        private string correo;
        private string conf_correo;
        private string telefono;
        private string conf_tel;
        private int cant_acce_fallidos;

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Contra
        {
            get
            {
                return contra;
            }

            set
            {
                contra = value;
            }
        }

        public string Conf_contra
        {
            get
            {
                return conf_contra;
            }

            set
            {
                conf_contra = value;
            }
        }

        public string Correo
        {
            get
            {
                return correo;
            }

            set
            {
                correo = value;
            }
        }

        public string Conf_correo
        {
            get
            {
                return conf_correo;
            }

            set
            {
                conf_correo = value;
            }
        }

        public string Telefono
        {
            get
            {
                return telefono;
            }

            set
            {
                telefono = value;
            }
        }

        public string Conf_tel
        {
            get
            {
                return conf_tel;
            }

            set
            {
                conf_tel = value;
            }
        }

        public int Cant_acce_fallidos
        {
            get
            {
                return cant_acce_fallidos;
            }

            set
            {
                cant_acce_fallidos = value;
            }
        }

        public Usuarios(int id, string nombre, string contra, string conf_contra, string correo, string conf_correo, string telefono, string conf_tel, int cant_acce_fallidos)
        {
            this.id = id;
            this.nombre = nombre;
            this.contra = contra;
            this.conf_contra = conf_contra;
            this.correo = correo;
            this.conf_correo = conf_correo;
            this.telefono = telefono;
            this.conf_tel = conf_tel;
            this.cant_acce_fallidos = cant_acce_fallidos;
        }
    }
}
